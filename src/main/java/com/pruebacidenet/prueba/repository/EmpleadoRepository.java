package com.pruebacidenet.prueba.repository;

import com.pruebacidenet.prueba.entity.EmpleadoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("EmpleadoRepo")
public interface EmpleadoRepository extends JpaRepository<EmpleadoEntity, Serializable>, PagingAndSortingRepository<EmpleadoEntity,Serializable> {

        public abstract EmpleadoEntity findByNumeroDocumentoAndTipoDocumento(String numeroDocumento, String tipoDocumento);
        public abstract Page<EmpleadoEntity> findAll(Pageable pageable);
        public abstract Page<EmpleadoEntity> findAllByNombre1(Pageable pageable,String nombre1);
        public abstract Page<EmpleadoEntity> findAllByNombre2(Pageable pageable,String nombre2);
        public abstract Page<EmpleadoEntity> findAllByApellido1(Pageable pageable,String apellido1);
        public abstract Page<EmpleadoEntity> findAllByApellido2(Pageable pageable,String apellido2);
        public abstract Page<EmpleadoEntity> findAllByTipoDocumento(Pageable pageable,String tipoDocumento);
        public abstract EmpleadoEntity findByNumeroDocumento(String numeroDocumento);
        public abstract Page<EmpleadoEntity> findAllByPaisEmpleo(Pageable pageable,String paisEmpleo);
        public abstract EmpleadoEntity findByCorreo(String correo);
        public abstract Page<EmpleadoEntity> findAllByEstado(Pageable pageable,boolean estado);

}
