package com.pruebacidenet.prueba.controller;
import com.pruebacidenet.prueba.model.Empleado;
import com.pruebacidenet.prueba.service.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin("*")
@RestController
@RequestMapping("index")
public class EmpleadoController {


    @Autowired
    private EmpleadoService empleadoService;

    @PostMapping("/insertar")
    public String insertar(@RequestBody Empleado empleado) {

        return this.empleadoService.insertarEmpleado(empleado);
    }

    @PutMapping("/actualizar")
    public String actualizar(@RequestBody Empleado empleado) {
        return this.empleadoService.actualizarEmpleado(empleado);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable long id) {
        return this.empleadoService.eliminarEmpleado(id);
    }

    @GetMapping("/listar")
    public List<Empleado> listar(Pageable pageable) {
        return this.empleadoService.listarEmpleados(pageable);
    }

    @GetMapping("listar/nombre1/{nombre1}")
    public List<Empleado> listarEmpleadosPorNombre1(Pageable pageable, @PathVariable("nombre1") String nombre1) {
        return this.empleadoService.listarEmpleadosPorNombre1(pageable, nombre1);
    }
    @GetMapping("listar/nombre2/{nombre2}")
    public List<Empleado> listarEmpleadosPorNombre2(Pageable pageable, @PathVariable("nombre2") String nombre2) {
        return this.empleadoService.listarEmpleadosPorNombre2(pageable, nombre2);
    }

    @GetMapping("listar/apellido1/{apellido1}")
    public List<Empleado> listarEmpleadosPorApellido1(Pageable pageable, @PathVariable("apellido1") String apellido1) {
        return this.empleadoService.listarEmpleadosPorApellido1(pageable, apellido1);
    }
    @GetMapping("listar/apellido2/{apellido2}")
    public List<Empleado> listarEmpleadosPorApellido2(Pageable pageable, @PathVariable("apellido2") String apellido2) {
        return this.empleadoService.listarEmpleadosPorApellido2(pageable, apellido2);
    }

    @GetMapping("listar/tipoDocumento/{tipoDocumento}")
    public List<Empleado> listarEmpleadosPorTipoDocumento(Pageable pageable, @PathVariable("tipoDocumento") String tipoDocumento) {
        return this.empleadoService.listarEmpleadosPorTipoDocumento(pageable, tipoDocumento);
    }
    @GetMapping("buscar/numeroDocumento/{numeroDocumento}")
    public Empleado getEmpleadoPorNumeroDocumento( @PathVariable("numeroDocumento") String numeroDocumento) {
        return this.empleadoService.getEmpledoPorNumeroDocumento( numeroDocumento);
    }

    @GetMapping("buscar/correo/{correo}")
    public Empleado getEmpleadoPorCorreo( @PathVariable("correo") String correo) {
        return this.empleadoService.getEmpledoPorCorreo( correo);
    }
    @GetMapping("listar/paisEmpleo/{paisEmpleo}")
    public List<Empleado> listarEmpleadosPorPaisEmpleo(Pageable pageable, @PathVariable("paisEmpleo") String paisEmpleo) {
        return this.empleadoService.listarEmpleadosPorPaisEmpleo(pageable, paisEmpleo);
    }
    @GetMapping("listar/estado/{estado}")
    public List<Empleado> listarEmpleadosPorEstado(Pageable pageable, @PathVariable("estado") boolean estado) {
        return this.empleadoService.listarEmpleadosPorEstado(pageable, estado);
    }

}
