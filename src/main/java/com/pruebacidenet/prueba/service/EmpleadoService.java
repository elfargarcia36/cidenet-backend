package com.pruebacidenet.prueba.service;

import com.pruebacidenet.prueba.converter.EmpleadoConverter;
import com.pruebacidenet.prueba.entity.EmpleadoEntity;
import com.pruebacidenet.prueba.model.Empleado;
import com.pruebacidenet.prueba.repository.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmpleadoService {

    @Autowired
    @Qualifier("EmpleadoRepo")
    public EmpleadoRepository empleadoRepository;

    @Autowired
    @Qualifier("convertidor")
    public EmpleadoConverter empleadoConverter;

    public String insertarEmpleado(Empleado empleado) {

        if (this.empleadoRepository.findByNumeroDocumentoAndTipoDocumento(empleado.getNumeroDocumento(),
                empleado.getTipoDocumento()) != null){
            return "ya existe";
        }

        EmpleadoEntity empleadoEntity = this.empleadoConverter.ConvertirListEntityToModel(empleado);
        try {
            this.empleadoRepository.save(empleadoEntity);
            return "registro exitoso";
        } catch (Exception e) {
            return "error";
        }
    }

    public String actualizarEmpleado(Empleado empleado) {


        EmpleadoEntity empleadoEntity = this.empleadoConverter.ConvertirListEntityToModel(empleado);
        try {
            this.empleadoRepository.save(empleadoEntity);
            return "registro exitoso";
        } catch (Exception e) {
            return "error";
        }
    }
     public String eliminarEmpleado(long id) {
         try {
             this.empleadoRepository.deleteById(id);
             return "Registro exitoso";
         } catch (Exception e) {
             return "No se pudo eliminar el empleado";
         }
     }


    public List<Empleado> listarEmpleados(Pageable pageable) {
        return this.empleadoConverter.ConvertirListEntityToModel(this.empleadoRepository.findAll(pageable).getContent());
    }

    public List<Empleado> listarEmpleadosPorNombre1(Pageable pageable, String nombre1) {
        return this.empleadoConverter.ConvertirListEntityToModel(
                this.empleadoRepository.findAllByNombre1(pageable, nombre1).getContent()
        );
    }

    public List<Empleado> listarEmpleadosPorNombre2(Pageable pageable, String nombre2) {
        return this.empleadoConverter.ConvertirListEntityToModel(
                this.empleadoRepository.findAllByNombre2(pageable, nombre2).getContent()
        );
    }
    public List<Empleado> listarEmpleadosPorApellido1(Pageable pageable, String apellido1) {
        return this.empleadoConverter.ConvertirListEntityToModel(
                this.empleadoRepository.findAllByApellido1(pageable, apellido1).getContent()
        );
    }

    public List<Empleado> listarEmpleadosPorApellido2(Pageable pageable, String apellido2) {
        return this.empleadoConverter.ConvertirListEntityToModel(
                this.empleadoRepository.findAllByApellido2(pageable, apellido2).getContent()
        );
    }

    public List<Empleado> listarEmpleadosPorTipoDocumento(Pageable pageable, String tipoDocumento) {
        return this.empleadoConverter.ConvertirListEntityToModel(
                this.empleadoRepository.findAllByTipoDocumento(pageable,tipoDocumento).getContent()
        );
    }

   public Empleado getEmpledoPorNumeroDocumento(String numeroDocumento ){
        return this.empleadoConverter
                .convertirEntityToModel(this.empleadoRepository.findByNumeroDocumento(numeroDocumento));
   }

    public List<Empleado> listarEmpleadosPorPaisEmpleo(Pageable pageable, String paisEmpleo) {
        return this.empleadoConverter.ConvertirListEntityToModel(
                this.empleadoRepository.findAllByPaisEmpleo(pageable,paisEmpleo).getContent()
        );
    }
    public Empleado getEmpledoPorCorreo(String correo ){
        return this.empleadoConverter.convertirEntityToModel(this.empleadoRepository.findByCorreo(correo));
    }

    public List<Empleado> listarEmpleadosPorEstado(Pageable pageable, boolean estado) {
        return this.empleadoConverter.ConvertirListEntityToModel(
                this.empleadoRepository.findAllByEstado(pageable,estado).getContent()
        );
    }
}
