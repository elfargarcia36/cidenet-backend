package com.pruebacidenet.prueba.converter;

import com.pruebacidenet.prueba.entity.EmpleadoEntity;
import com.pruebacidenet.prueba.model.Empleado;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("convertidor")
public class EmpleadoConverter {

       public EmpleadoEntity ConvertirListEntityToModel(Empleado empleado) {

        return new EmpleadoEntity(empleado.getId(),
                                 empleado.getNombre1(),
                                 empleado.getNombre2(),
                                 empleado.getApellido1(),
                                 empleado.getApellido2(),
                                 empleado.getTipoDocumento(),
                                 empleado.getNumeroDocumento(),
                                 empleado.getPaisEmpleo(),
                                 empleado.getCorreo(),
                                 empleado.getFechaIngreso(),
                                 empleado.getFechaRegistro(),
                                 empleado.getFechaEdicion(),
                                 empleado.isEstado()
        );
    }

    public Empleado convertirEntityToModel(EmpleadoEntity empleadoEntity){
      return new Empleado(empleadoEntity.getId(),
              empleadoEntity.getNombre1(),
              empleadoEntity.getNombre2(),
              empleadoEntity.getApellido1(),
              empleadoEntity.getApellido2(),
              empleadoEntity.getTipoDocumento(),
              empleadoEntity.getNumeroDocumento(),
              empleadoEntity.getPaisEmpleo(),
              empleadoEntity.getCorreo(),
              empleadoEntity.getFechaIngreso(),
              empleadoEntity.getFechaRegistro(),
              empleadoEntity.getFechaEdicion(),
              empleadoEntity.isEstado()
      );
    }

    public List<Empleado> ConvertirListEntityToModel(List<EmpleadoEntity> empleadoEntities) {
        List<Empleado> empleados = new ArrayList<>();
        empleadoEntities.stream().forEach(empleadoEntity -> {
             empleados.add(new Empleado(empleadoEntity.getId(),
                                        empleadoEntity.getNombre1(),
                                        empleadoEntity.getNombre2(),
                                        empleadoEntity.getApellido1(),
                                        empleadoEntity.getApellido2(),
                                        empleadoEntity.getTipoDocumento(),
                                        empleadoEntity.getNumeroDocumento(),
                                        empleadoEntity.getPaisEmpleo(),
                                        empleadoEntity.getCorreo(),
                                        empleadoEntity.getFechaIngreso(),
                                        empleadoEntity.getFechaRegistro(),
                                        empleadoEntity.getFechaEdicion(),
                                        empleadoEntity.isEstado()
             ));
        });
        return empleados;
    }

}
