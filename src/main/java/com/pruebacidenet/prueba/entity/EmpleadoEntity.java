package com.pruebacidenet.prueba.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="empleado")
public class EmpleadoEntity implements Serializable {

    @GeneratedValue
    @Id
    @Column(name="id")
    private long id;

    @Column(name="nombre1", length = 20)
    private String nombre1;

    @Column(name="nombre2",length = 50)
    private String nombre2;

    @Column(name="apellido1",length = 20)
    private String apellido1;

    @Column(name="apellido2",length = 20)
    private String apellido2;

    @Column(name="tipoDocumento")
    private String tipoDocumento;

    @Column(name="numeroDocumento",length = 20)
    private String numeroDocumento;

    @Column(name="paisEmpleo")
    private String paisEmpleo;

    @Column(name="correo",length = 300)
    private String correo;

    @Column(name="fechaIngreso")
    private Date fechaIngreso;

    @Column(name="fechaRegistro")
    private Date fechaRegistro;

    @Column(name="fechaEdicion")
    private Date fechaEdicion;

    @Column(name="estado")
    private boolean estado;

    public EmpleadoEntity() {
    }

    public EmpleadoEntity(long id, String nombre1, String nombre2, String apellido1, String apellido2, String tipoDocumento, String numeroDocumento,
                           String paisEmpleo, String correo,Date fechaIngreso,Date fechaRegistro,Date fechaEdicion,boolean estado) {
        this.id = id;
        this.nombre1 = nombre1;
        this.nombre2 = nombre2;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.paisEmpleo = paisEmpleo;
        this.correo = correo;
        this.fechaIngreso=fechaIngreso;
        this.fechaRegistro=fechaRegistro;
        this.fechaEdicion=fechaEdicion;
        this.estado=estado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getPaisEmpleo() {
        return paisEmpleo;
    }

    public void setPaisEmpleo(String paisEmpleo) {
        this.paisEmpleo = paisEmpleo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaEdicion() {
        return fechaEdicion;
    }

    public void setFechaEdicion(Date fechaEdicion) {
        this.fechaEdicion = fechaEdicion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
